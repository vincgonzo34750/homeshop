package tests;

import homeshop_main.Delivery;
import homeshop_main.RelayDelivery;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RelayDeliveryTest {
    @Test
    public void Given_FreeRelayPrice_withRelayNbr(){
        Delivery delivery = new RelayDelivery(5);
        assertEquals(0.0, delivery.getPrice(), 0.01);
    }

    @Test
    public void Given_LowRelayPrice_withRelayNbr(){
        Delivery delivery = new RelayDelivery(27);
        assertEquals(2.99, delivery.getPrice(), 0.01);
    }

    @Test
    public void Given_HighRelayPrice_withRelayNbr(){
        Delivery delivery = new RelayDelivery(57);
        assertEquals(4.99, delivery.getPrice(), 0.01);
    }
}