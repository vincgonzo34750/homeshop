package homeshop_main;

public interface Delivery {
    double getPrice();
}
