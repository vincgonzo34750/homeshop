package homeshop_main;

import java.util.Map;

public class Product {
    private String Name;
    private String Description;
    private double Price;

    public Product(String name, String description, double price) {
        Name = name;
        Description = description;
        Price = price;
    }

    public String getName() {
        return Name;
    }

    public String getDescription() {
        return Description;
    }

    public void setPrice(double price) {
        Price = price;
    }

    public double getPrice() {
        return Price;
    }

    /**
     * Display full desc of product
     */
    public void look(){

    }

    /**
     * Add a product to a bill
     * @param bill concerned Bill obj
     * @param quantity qty of product to add
     */
    public void buy(Bill bill, Integer quantity){

    }
}
