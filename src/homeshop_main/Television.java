package homeshop_main;

public class Television extends Product {
    private int Size;
    private String slabType;

    public Television(String name, String description, double price, int size, String slabType) {
        super(name, description, price);
        Size = size;
        this.slabType = slabType;
    }

    public int getSize() {
        return Size;
    }

    public String getSlabType() {
        return slabType;
    }
}
