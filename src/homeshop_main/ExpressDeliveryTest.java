package homeshop_main;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExpressDeliveryTest {
    @Test
    public void Given_PriceByRegionNotParis(){
        Delivery delivery = new ExpressDelivery("Bordeaux");
        assertEquals(9.99, delivery.getPrice(), 0.01);
    }

    @Test
    public void Given_PriceByRegionIsParis(){
        Delivery delivery = new ExpressDelivery("Paris");
        assertEquals(6.99, delivery.getPrice(), 0.01);
    }
}